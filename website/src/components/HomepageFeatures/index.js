import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Docker Swarm',
    Svg: require('@site/static/img/docker.svg').default,
    description: (
      <>
        Docker swarm e uma das formas mais simples de gerenciar seus containers de forma clusterizadas. Eu acho uma
        muito boa e pratica de criar e gerenciar seus cluters sem muita dor de cabeca.
      </>
    ),
  },
  {
    title: 'Kubernetes',
    Svg: require('@site/static/img/kubernetes.svg').default,
    description: (
      <>
        Kubernetes e uma das ferramentas mais poderosas para o gerenciamento de containers. Isso e facil de
        perceber porque as principais plataformas de nuvem o utilizam. Vamos aprender mais sobre esse cara.
      </>
    ),
  },
  /*{
    title: 'Powered by React',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        Extend or customize your website layout by reusing React. Docusaurus can
        be extended while reusing the same header and footer.
      </>
    ),
  },*/
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
