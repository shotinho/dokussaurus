# Docker Swarm

O Docker Swarm e uma ferramenta de orquestracao de *container*. E um pouco semelhante ao *Kubernetes* porque tambem tenta orquestrar os *containers*. E recomendado usar o *Docker Swarm* para ambientes de pequeno e ate medio porte. Se em seu ambiente existe algo com uma maior complexidade, e interessante adotar o *Kubernetes*.

## Instalacao

<p>A instalacao do *Docker Swarm* e feita quando instalamos o *Docker*, ou seja, basta instalar o Docker que o Swarm ja vem por padrao nesta instalacao.</p>

